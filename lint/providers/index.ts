import tslint from './tslint';
import tslintConfigSecurity from './tslint-config-security';
import tslintEslintRules from './tslint-eslint-rules';
import tslintSonarts from './tslint-sonarts';

export default {
	...tslint,
	...tslintConfigSecurity,
	...tslintEslintRules,
	...tslintSonarts,
};
