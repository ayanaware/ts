export default {
	'array-bracket-spacing': [
		true,
		'never',
	],
	'handle-callback-err': {
		severity: 'warning',
	},
	'no-constant-condition': true,
	'no-control-regex': true,
	'no-duplicate-case': true,
	'no-empty-character-class': true,
	'no-ex-assign': true,
	'no-extra-boolean-cast': true,
	'no-extra-semi': true,
	'no-inner-declarations': [
		true,
		'functions',
	],
	'no-invalid-regexp': true,
	'no-multi-spaces': [
		true,
		{
			exceptions: {
				PropertyAssignment: true,
			},
		},
	],
	'no-regex-spaces': true,
	'no-unexpected-multiline': true,
	'object-curly-spacing': [
		true,
		'always',
	],
	'ter-arrow-parens': [
		true,
		'as-needed',
	],
	'ter-arrow-spacing': [
		true,
		{
			after: true,
			before: true,
		},
	],
	'ter-func-call-spacing': [
		true,
		'never',
	],
	'ter-indent': [
		true,
		'tab',
		{
			CallExpression: {
				arguments: 1,
			},
			FunctionDeclaration: {
				body: 1,
				parameters: 1,
			},
			FunctionExpression: {
				body: 1,
				parameters: 1,
			},
			SwitchCase: 1,
		},
	],
	'ter-no-self-compare': true,
	'ter-padded-blocks': [
		true,
		'never',
	],
	'valid-jsdoc': {
		options: {
			requireParamDescription: true,
			requireParamType: false,
			requireReturn: false,
			requireReturnDescription: true,
			requireReturnType: false,
		},
		severity: 'warning',
	},
	'valid-typeof': true,
};
