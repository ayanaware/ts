export default {
	'tsr-detect-buffer-noassert': true,
	'tsr-detect-non-literal-buffer': true,
	'tsr-detect-non-literal-regexp': true,
	'tsr-detect-non-literal-require': true,
	'tsr-detect-pseudo-random-bytes': true,
	'tsr-detect-sql-literal-injection': true,
	'tsr-detect-unsafe-regexp': true,
};
