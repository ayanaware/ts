@ayanaware/ts [![NPM](https://img.shields.io/npm/v/@ayanaware/ts.svg)](https://www.npmjs.com/package/@ayanaware/ts) [![Discord](https://discordapp.com/api/guilds/508903834853310474/embed.png)](https://discord.gg/eaa5pYf)
===

TypeScript config, linting and compiler helper for Ayana projects.

Install
---

```bash
yarn add @ayanaware/ts tslint typescript --dev
```

```bash
npm i @ayanaware/ts tslint typescript --only=dev
```

Use
---

To use the TSConfig, add the following to your `tsconfig.json`:

```json
{
	"extends": "@ayanaware/ts/config"
}
```

**NOTE:** Using this TSConfig will make `tsc` unusable by itself. You have to use `ayc`, either in the command line by installing it globally or as a build script inside your `package.json`.


To use the TSLint config, add the following to your `tslint.json`:

```json
{
	"extends": "@ayanaware/ts/lint"
}
```
